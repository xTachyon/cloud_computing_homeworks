mod database;

use anyhow::Result;
use derive_new::new;
use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Method, Request, Response, Server, StatusCode};
use serde_derive::Deserialize;
use serde_derive::Serialize;
use std::collections::HashMap;
use std::sync::Arc;
use tokio::fs;
use tokio_postgres::{Client, NoTls};
use crate::database::{search_domains_by_id, search_categories_by_name};

macro_rules! database_error {
    ($expr:expr, $lit:literal) => {{
        match $expr {
            Ok(x) => x,
            Err(err) => { println!("{:?}", err); return generic_error($lit, StatusCode::SERVICE_UNAVAILABLE) }
        }
    }};
}

#[derive(new)]
struct MyResponse {
    json: String,
    status_code: StatusCode,
}

#[derive(Serialize)]
struct GenericError {
    error: String,
}

fn generic_error(string: &str, status_code: StatusCode) -> Result<MyResponse> {
    let error = GenericError {
        error: string.into(),
    };
    let response = MyResponse {
        json: serde_json::to_string_pretty(&error).unwrap(),
        status_code,
    };
    Ok(response)
}

fn unknown_route() -> Result<MyResponse> {
    let json = r#"{ "error": "unknown route" }"#;
    Ok(MyResponse::new(json.into(), StatusCode::NOT_FOUND))
}

async fn get_domains_by_name(server_data: ServerData, name: &str) -> Result<MyResponse> {
    let domains = database::search_domains_by_name(&server_data.database, name).await?;
    let json = serde_json::to_string_pretty(&domains).unwrap_or_default();
    Ok(MyResponse::new(json, StatusCode::OK))
}

async fn get_domains_by_id(server_data: ServerData, id: &str) -> Result<MyResponse> {
    let id: i64 = match id.parse() {
        Ok(x) => x,
        Err(_) => return generic_error("couldn't parse id as unsigned", StatusCode::UNPROCESSABLE_ENTITY)
    };
    let domain = search_domains_by_id(&server_data.database, id).await?;
    if let None = domain {
        return generic_error("not found", StatusCode::NOT_FOUND)
    }
    let json = serde_json::to_string_pretty(&domain).unwrap_or_default();
    Ok(MyResponse::new(json, StatusCode::OK))
}

async fn domains_get(
    server_data: ServerData,
    params: HashMap<String, String>,
) -> Result<MyResponse> {
    if let Some((_, name)) = params.get_key_value("name") {
        return get_domains_by_name(server_data, name).await;
    }
    if let Some((_, id)) = params.get_key_value("id") {
        return get_domains_by_id(server_data, id).await;
    }

    generic_error("invalid params", StatusCode::UNPROCESSABLE_ENTITY)
}

async fn domains_delete(
    server_data: ServerData,
    params: HashMap<String, String>,
) -> Result<MyResponse> {
    if let Some((_, id)) = params.get_key_value("id") {
        let result = database::delete_domain_by_id(&server_data.database, id.parse()?).await;
        let result = database_error!(result, "can't delete domain by id");
        if let None = result {
            return generic_error("not found", StatusCode::NOT_FOUND)
        }
        let json = serde_json::to_string_pretty(&result).unwrap_or_default();
        return Ok(MyResponse::new(json, StatusCode::OK))
    }

    if let Some((_, name)) = params.get_key_value("name") {
        let result = database::delete_domain_by_name(&server_data.database, name).await;
        let result = database_error!(result, "can't delete domain by name");
        let json = serde_json::to_string_pretty(&result).unwrap_or_default();
        return Ok(MyResponse::new(json, StatusCode::OK))
    }

    generic_error("invalid params", StatusCode::UNPROCESSABLE_ENTITY)
}

#[derive(Serialize, Deserialize)]
enum DomainPostRequest {
    Add(String)
}

async fn domains_post(server_data: ServerData, body: &str) -> Result<MyResponse> {
    let request: DomainPostRequest = match serde_json::from_str(body) {
        Ok(x) => x,
        Err(_) => return generic_error("can't parse request", StatusCode::PRECONDITION_FAILED)
    };

    match request {
        DomainPostRequest::Add(x) => {
            let result = database_error!(database::add_domain(&server_data.database, &x).await, "can't add domain");
            let json = serde_json::to_string_pretty(&result).unwrap_or_default();
            return Ok(MyResponse::new(json, StatusCode::CREATED))
        },
    };

}

#[derive(Deserialize)]
struct DomainPutRequest {
    id: i64,
    new: String
}

async fn domains_put(server_data: ServerData, body: &str) -> Result<MyResponse> {
    let request: DomainPutRequest = match serde_json::from_str(body) {
        Ok(x) => x,
        Err(_) => return generic_error("can't parse request", StatusCode::PRECONDITION_FAILED)
    };

    let result = database::update_domain(&server_data.database, request.id, &request.new).await;
    database_error!(result, "can't update");
    Ok(MyResponse::new("".into(), StatusCode::NO_CONTENT))
}

#[derive(Serialize, Deserialize)]
struct CategoryPostRequestAdd {
    category: String,
    domains: Vec<i64>
}

#[derive(Serialize, Deserialize)]
enum CategoryPostRequest {
    Add(CategoryPostRequestAdd),
    Replace(CategoryPostRequestAdd)
}

async fn add_category(server_data: ServerData, add: CategoryPostRequestAdd) -> Result<MyResponse> {
    let category_id = database_error!(database::add_category(&server_data.database, &add.category).await, "can't add category");
    for domain in add.domains {
        database_error!(database::add_category_mapping(&server_data.database, category_id, domain).await, "can't add category mapping");
    }

    Ok(MyResponse::new("".into(), StatusCode::OK))
}

async fn get_category_by_name(server_data: ServerData, name: &str) -> Result<MyResponse> {
    let domain = search_categories_by_name(&server_data.database, name).await?;
    let json = serde_json::to_string_pretty(&domain).unwrap_or_default();
    Ok(MyResponse::new(json, StatusCode::OK))
}

async fn category_get(
    server_data: ServerData,params: HashMap<String, String>
) -> Result<MyResponse> {
    if let Some((_, name)) = params.get_key_value("name") {
        return get_category_by_name(server_data, name).await;
    }

    generic_error("invalid params", StatusCode::UNPROCESSABLE_ENTITY)
}

async fn replace_category(server_data: ServerData, replace: CategoryPostRequestAdd) -> Result<()> {
    database::delete_category(&server_data.database, &replace.category).await?;
    add_category(server_data, replace).await?;

    Ok(())
}

async fn category_put(
    server_data: ServerData,
    body: &str
) -> Result<MyResponse> {
    let request: CategoryPostRequest = match serde_json::from_str(body) {
        Ok(x) => x,
        Err(_) => return generic_error("can't parse request", StatusCode::PRECONDITION_FAILED)
    };

    match request {
        CategoryPostRequest::Replace(x) => {
            replace_category(server_data, x).await?;
            return Ok(MyResponse::new("".into(), StatusCode::CREATED))
        },
        _ => {}
    };

    generic_error("unknown category request type", StatusCode::NOT_FOUND)
}

async fn category_post(
    server_data: ServerData,
    body: &str
) -> Result<MyResponse> {
    let request: CategoryPostRequest = match serde_json::from_str(body) {
        Ok(x) => x,
        Err(_) => return generic_error("can't parse request", StatusCode::PRECONDITION_FAILED)
    };

    match request {
        CategoryPostRequest::Add(x) => return add_category(server_data, x).await,
        _ => {}
    };

    generic_error("unknown category request type", StatusCode::NOT_FOUND)
}

fn parse_parameters(string: &str) -> HashMap<String, String> {
    let mut map = HashMap::new();
    for option in string.split('&') {
        let mut tokens = option.split('=');
        let first: &str = tokens.next().unwrap_or_default();
        let second: &str = tokens.next().unwrap_or_default();
        if first.is_empty() || second.is_empty() {
            continue;
        }
        map.entry(first.into()).or_insert(second.into());
    }

    map
}

async fn resolve_request(
    mut request: Request<Body>,
    server_data: ServerData,
) -> Result<Response<Body>> {
    let params = parse_parameters(request.uri().query().unwrap_or_default());
    let body = hyper::body::to_bytes(request.body_mut()).await?;
    let body = std::str::from_utf8(&body)?;

    println!("{:?} {:?}", request.method(), request.uri());
    let result = match (request.method().clone(), request.uri().path()) {
        (Method::GET, "/domain") => domains_get(server_data, params).await,
        (Method::DELETE, "/domain") => domains_delete(server_data, params).await,
        (Method::POST, "/domain") => domains_post(server_data, body).await,
        (Method::PUT, "/domain") => domains_put(server_data, body).await,
        (Method::GET, "/category") => category_get(server_data, params).await,
        (Method::POST, "/category") => category_post(server_data, body).await,
        (Method::PUT, "/category") => category_put(server_data, body).await,
        _ => unknown_route(),
    };

    let (json, status_code) = match result {
        Ok(ok) => (ok.json, ok.status_code),
        Err(err) => {
            let error = GenericError {
                error: format!("{:?}", err),
            };
            let error = serde_json::to_string_pretty(&error).unwrap();
            (error, StatusCode::INTERNAL_SERVER_ERROR)
        }
    };

    let body = Body::from(json);
    let result = Response::builder().status(status_code).body(body)?;
    Ok(result)
}

#[derive(Deserialize)]
struct Configuration {
    database_string: String,
}

async fn read_config() -> Result<Configuration> {
    let string = fs::read_to_string("config.toml").await?;
    Ok(toml::from_str(&string)?)
}

struct ServerDataStruct {
    database: Client,
    // config: Configuration,
}

type ServerData = Arc<ServerDataStruct>;

#[tokio::main]
async fn main() -> Result<()> {
    let config = read_config().await?;
    let (client, connection) = tokio_postgres::connect(&config.database_string, NoTls).await?;

    tokio::spawn(async move {
        if let Err(e) = connection.await {
            eprintln!("connection error: {}", e);
        }
    });

    let server_data = Arc::new(ServerDataStruct {
        database: client,
        // config,
    });

    let address = "127.0.0.1:80".parse()?;
    let new_service = make_service_fn(move |_| {
        let server_data = server_data.clone();
        async {
            Ok::<_, anyhow::Error>(service_fn(move |request| {
                resolve_request(request, server_data.clone())
            }))
        }
    });

    let server = Server::bind(&address).serve(new_service);

    println!("Listening on http://{}", address);

    server.await?;
    Ok(())
}
