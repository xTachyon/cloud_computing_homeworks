﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Api.Gax.ResourceNames;
using Google.Cloud.Datastore.V1;
using Google.Cloud.Storage.V1;
using Google.Cloud.TextToSpeech.V1;
using Google.Cloud.Translate.V3;
using Google.Cloud.Vision.V1;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Homework3.Controllers {
    public class AddRequest {
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class AddResponse {
        public string Uuid { get; set; }
        public string UploadUrl { get; set; }
    }

    public class ErrorResponse {
        public string Error { get; set; }
    }

    public class QueryResponse {
        public string Uuid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Voice { get; set; }
        public DateTime Created { get; set; }
        public string Tags { get; set; }
    }

    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase {
        private const string DATASTORE_NAMESPACE = "mynamesapce";

        private const string BASE_URL = "http://localhost:5000";
        //private const string BASE_URL = "https://claud-12345.appspot.com";

        [HttpGet("{*url}", Order = int.MaxValue)]
        public ErrorResponse CatchAll() {
            Response.StatusCode = StatusCodes.Status404NotFound;
            return new ErrorResponse {
                Error = "what are you trying to do ?"
            };
        }

        [HttpPost("add")]
        public async Task<AddResponse> Add([FromBody] AddRequest request) {
            var db = DatastoreDb.Create(Program.GcpProjectId, DATASTORE_NAMESPACE);
            var keyFactory = db.CreateKeyFactory("files");
            var uuid = Guid.NewGuid().ToString();
            var entity = new Entity {
                Key = keyFactory.CreateKey(uuid),
                ["uuid"] = uuid,
                ["name"] = request.Name,
                ["description"] = request.Description,
                ["created"] = DateTime.UtcNow,
            };

            using (var transaction = await db.BeginTransactionAsync()) {
                transaction.Insert(entity);
                var response = await transaction.CommitAsync();
            }

            return new AddResponse {
                Uuid = uuid,
                UploadUrl = $"/api/file/upload/uuid/{uuid}"
            };
        }

        private bool IsImage(string name) {
            return new[] { ".jpg", ".png" }
                .Any(x => name.EndsWith(x));
        }

        private async Task<string> GetTags(byte[] bytes) {
            try {
                var image = Image.FromBytes(bytes);

                var client = await ImageAnnotatorClient.CreateAsync();
                var labels = await client.DetectLabelsAsync(image);
                var result = string.Join("\n", labels
                    .Select(x => $"name={x.Description} --- score={(int)(x.Score * 100)}%"));
                if (result.Length == 0) {
                    result = "who knows";
                }
                return result;
            } catch (Exception) {
                return "who knows";
            }
        }

        [HttpPost("upload/uuid/{uuid}")]
        public async Task<object> Upload(string uuid) {
            var db = DatastoreDb.Create(Program.GcpProjectId, DATASTORE_NAMESPACE);
            var query = new Query("files") {
                Filter = Filter.Equal("uuid", uuid)
            };
            var results = (await db.RunQueryAsync(query)).Entities.ToList();
            if (results.Count == 0) {
                return new ErrorResponse {
                    Error = "no file with that uuid known"
                };
            }
            var result = results[0];
            if (result["tags"].OrNull() != null) {
                return new ErrorResponse {
                    Error = "file already uploaded"
                };
            }
            var name = result["name"];

            var client = await StorageClient.CreateAsync();
            var bytesStream = new MemoryStream();
            await Request.Body.CopyToAsync(bytesStream);
            var bytes = bytesStream.ToArray();
            var tags = await GetTags(bytes);

            result["tags"] = tags;

            using (var transaction = await db.BeginTransactionAsync()) {
                transaction.Update(result);
                var response = await transaction.CommitAsync();
            }

            await client.UploadObjectAsync(DATASTORE_NAMESPACE, uuid, "application/x-binary", new MemoryStream(bytes));

            return new ErrorResponse {
                Error = "success"
            };
        }

        [HttpGet("data/name/{name}")]
        public async Task<ActionResult> Data(string name) {
            var client = await StorageClient.CreateAsync();

            var stream = new MemoryStream();
            await client.DownloadObjectAsync(DATASTORE_NAMESPACE, name, stream);
            var array = stream.ToArray();

            return File(array, "application/octet-stream");
        }

        [HttpGet("query/uuid/{uuid}/language/{language}")]
        public async Task<object> Query(string uuid, string language = "en-us") {
            var db = DatastoreDb.Create(Program.GcpProjectId, DATASTORE_NAMESPACE);
            var query = new Query("files") {
                Filter = Filter.Equal("uuid", uuid)
            };
            var results = (await db.RunQueryAsync(query)).Entities.ToList();

            if (results.Count == 0) {
                return new ErrorResponse {
                    Error = "no file with that uuid"
                };
            }
            var result = results[0];

            var description = result["description"].StringValue;
            var descriptionTranslated = await Translate(description, language);

            var speakBytes = await Speak(descriptionTranslated, language);
            string speakUrl = null;
            if (speakBytes != null) {
                var name = $"{uuid}_{language}_voice.mp3";
                speakUrl = "/api/file/data/name/" + name;
                await UploadFile(name, speakBytes);
            }

            return new QueryResponse {
                Uuid = result["uuid"].StringValue,
                Name = result["name"].StringValue,
                Description = descriptionTranslated,
                Created = result["created"].TimestampValue.ToDateTime(),
                Voice = speakUrl,
                Tags = result["tags"].OrNull() == null ? null : result["tags"].StringValue
            };
        }

        [HttpGet("last/{n}/language/{language}")]
        public async Task<object> Last(string n, string language = "en") {
            var db = DatastoreDb.Create(Program.GcpProjectId, DATASTORE_NAMESPACE);
            var query = new Query("files") {
                Limit = int.Parse(n),
                Order = { { "created", PropertyOrder.Types.Direction.Descending } }
            };
            var queryResult = await db.RunQueryAsync(query);
            var result = new List<object>();
            foreach (var i in queryResult.Entities) {
                var queried = await Query(i["uuid"].StringValue, language);
                result.Add(queried);
            }
            return result;
        }

        public async Task UploadFile(string name, byte[] bytes) {
            var client = await StorageClient.CreateAsync();

            await client.UploadObjectAsync(DATASTORE_NAMESPACE, name, "application/x-binary", new MemoryStream(bytes));
        }

        private async Task<byte[]> Speak(string text, string language) {
            try {
                var input = new SynthesisInput {
                    Text = text
                };
                var voice = new VoiceSelectionParams {
                    LanguageCode = language,
                    SsmlGender = new Random().Next() % 2 == 0 ? SsmlVoiceGender.Female : SsmlVoiceGender.Male
                };
                var audioConfig = new AudioConfig {
                    AudioEncoding = AudioEncoding.Mp3,
                    SpeakingRate = 1.25
                };

                var client = await TextToSpeechClient.CreateAsync();
                var response = await client.SynthesizeSpeechAsync(input, voice, audioConfig);
                var bytes = response.AudioContent.ToArray();
                return bytes;
            } catch (Exception) {
                return null;
            }
        }

        private async Task<string> Translate(string text, string language) {
            try {
                var client = await TranslationServiceClient.CreateAsync();
                var request = new TranslateTextRequest {
                    Contents = { text },
                    TargetLanguageCode = language,
                    Parent = new ProjectName(Program.GcpProjectId).ToString(),
                    MimeType = "text/plain"
                };

                var response = await client.TranslateTextAsync(request);
                return response.Translations[0].TranslatedText;
            } catch (Exception) {
                return null;
            }
        }
    }
}
