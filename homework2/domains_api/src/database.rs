use anyhow::Result;
use serde_derive::Serialize;
use tokio_postgres::{Client, Row};

#[derive(Debug, Serialize)]
pub struct DomainInfo {
    id: i64,
    dns: String,
}

fn row_to_domain_info(row: Row) -> DomainInfo {
    DomainInfo {
        id: row.get(0),
        dns: row.get(1),
    }
}

pub async fn search_domains_by_name(database: &Client, search: &str) -> Result<Vec<DomainInfo>> {
    let search = format!("%{}%", search);
    let rows = database
        .query(
            "select domain_id, dns from public.domains where dns like $1",
            &[&search],
        )
        .await?;
    let rows = rows
        .into_iter()
        .map(row_to_domain_info)
        .collect();

    Ok(rows)
}

pub async fn search_domains_by_id(database: &Client, id: i64) -> Result<Option<DomainInfo>> {
    let rows = database
        .query(
            "select domain_id, dns from public.domains where domain_id = $1",
            &[&id],
        )
        .await?;

    let row = rows
        .into_iter()
        .map(row_to_domain_info)
        .next();

    Ok(row)
}

pub async fn add_category(database: &Client, category: &str) -> Result<i64> {
    let _ = database.query("insert into categories (category_name) values ($1)", &[&category]).await;
    let id = database.query_one("select category_id from categories where category_name = $1", &[&category]).await?;
    println!("{:?}", id);

    Ok(id.get("category_id"))
}

pub async fn add_category_mapping(database: &Client, category: i64, domain: i64) -> Result<()> {
    database.query("insert into category_mapping (domain_id, category_id) values ($1, $2)", &[&domain, &category]).await?;
    Ok(())
}

#[derive(Debug, Serialize)]
pub struct CategoryMapping {
    category_id: i64,
    category_name: String,
    domains: Vec<DomainInfo>
}

pub async fn search_categories_by_name(database: &Client, name: &str) -> Result<CategoryMapping> {
    let row = database
        .query_one(
            "select category_id, category_name from categories where category_name = $1",
            &[&name],
        )
        .await?;

    let category_id = row.get("category_id");
    let category_name = row.get("category_name");

    let rows = database.query("select domain_id from category_mapping where category_id = $1", &[&category_id]).await?;

    let mut domains = Vec::new();
    for i in rows {
        let domain = search_domains_by_id(database, i.get(0)).await?;
        if let Some(x) = domain {
            domains.push(x);
        }
    }

    let result = CategoryMapping {
        category_id,
        category_name,
        domains
    };

    Ok(result)
}

pub async fn add_domain(database: &Client, name: &str) -> Result<Option<DomainInfo>> {
    let row = database.query_one("insert into domains (dns) values ($1) returning domain_id", &[&name]).await?;
    search_domains_by_id(database, row.get(0)).await
}

pub async fn delete_domain_by_id(database: &Client, id: i64) -> Result<Option<DomainInfo>> {
    let domain = search_domains_by_id(database, id).await?;
    database.query("delete from domains where domain_id = $1", &[&id]).await?;
    Ok(domain)
}

pub async fn delete_domain_by_name(database: &Client, name: &str) -> Result<Vec<DomainInfo>> {
    let domains = search_domains_by_name(database, name).await?;

    let like = format!("%{}%", name);
    database.query("delete from domains where dns like $1", &[&like]).await?;
    Ok(domains)
}

async fn get_category_by_name(database: &Client, name: &str) -> Result<i64> {
    let row = database.query_one("select category_id from categories where category_name = $1", &[&name]).await?;
    Ok(row.get(0))
}

pub async fn delete_category(database: &Client, name: &str) -> Result<()> {
    let category_id = get_category_by_name(database, name).await?;
    database.query("delete from category_mapping where category_id = $1", &[&category_id]).await?;
    database.query("delete from categories where category_id = $1", &[&category_id]).await?;

    Ok(())
}

pub async fn update_domain(database: &Client, id: i64, name: &str) -> Result<()> {
    database.query("insert into domains (domain_id, dns) values ($1, $2)\
    on conflict (domain_id) do update set dns = $3 where domains.domain_id = $4", &[&id, &name, &name, &id]).await?;

    Ok(())
}