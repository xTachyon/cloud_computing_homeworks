use anyhow::Result;
use sqlx::PgPool;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::time::Instant;
use xz2::read::XzDecoder;

async fn do_one(file: File, pool: &PgPool) -> Result<()> {
    let reader = XzDecoder::new(file);
    let reader = BufReader::new(reader);
    let mut connection = pool.acquire().await?;

    let mut last = Instant::now();

    for (count, line) in reader.lines().enumerate() {
        let line = line?;
        let line = line.trim();
        if line.is_empty() {
            continue;
        }
        let sql = format!("insert into domains (dns) values ('{}');", line);

        sqlx::query(&sql).execute(&mut connection).await?;

        if last.elapsed().as_secs() >= 1 {
            last = Instant::now();
            println!("count: {}", count);
        }
    }

    Ok(())
}

async fn create_tables(pool: &PgPool) -> Result<()> {
    let create_tables_sql = include_str!("create_tables.sql");
    let mut connection = pool.acquire().await?;

    let iterator = create_tables_sql.split(';').filter(|x| !x.is_empty());

    for sql in iterator {
        sqlx::query(sql).execute(&mut connection).await?;
    }

    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    let pool = PgPool::new("postgres://domains:domains@localhost/domains").await?;
    create_tables(&pool).await?;

    let path = r#"D:\cc\2nd"#;
    let countries = ["ro"];

    for i in countries.iter() {
        let entry = format!("{}/{}.txt.xz", path, i);
        let file = File::open(entry)?;
        do_one(file, &pool).await?;
    }

    Ok(())
}
