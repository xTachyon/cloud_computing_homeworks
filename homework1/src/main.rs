use anyhow::anyhow;
use anyhow::Result;
use futures::lock::Mutex;
use futures::stream::FuturesUnordered;
use futures::TryStreamExt;
use hyper::body::{Bytes, Sender};
use hyper::service::make_service_fn;
use hyper::service::service_fn;
use hyper::Body;
use hyper::Request;
use hyper::Response;
use hyper::Server;
use reqwest::header::{HeaderMap, HeaderValue};
use reqwest::{header, Client as ReqwestClient};
use serde::de::DeserializeOwned;
use serde_derive::Deserialize;
use serde_derive::Serialize;
use std::collections::HashMap;
use std::sync::Arc;
use std::time::Instant;
use tokio::fs;
use url::form_urlencoded;

const STEAM_GET_USER_INFO_URL: &str =
    "https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key={steam_key}&steamids={steam_id}";

const STEAM_GET_OWNED_GAMES_URL: &str =
    "https://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key={steam_key}\
&steamid={steam_id}&format=json&include_appinfo=true&include_played_free_games=true";

const ITAD_URL: &str = "https://api.isthereanydeal.com/v01/search/search/";

const SENDGRID_SEND_URL: &str = "https://api.sendgrid.com/v3/mail/send";

#[derive(Deserialize)]
struct Configuration {
    steam_key: String,
    itad_key: String,
    sendgrid_key: String,
    email_address: String,
    listen_on: String,
    test_steam_id: u64,
    batch_number: u16,
    number_of_batches: u16,
}

#[derive(Debug, Serialize, Deserialize)]
struct ItadGameInfoUrls {
    game: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct ItadGameInfo {
    title: String,
    price_new: f64,
    urls: ItadGameInfoUrls,
}

#[derive(Deserialize)]
struct ItadSearchResponseData {
    list: Vec<ItadGameInfo>,
}

#[derive(Deserialize)]
struct ItadSearchResponse {
    data: ItadSearchResponseData,
}

async fn do_get_request<T: DeserializeOwned>(client: ReqwestClient, url: &str) -> Result<T> {
    let text = client.get(url).send().await?.text().await?;
    println!("url={}\n{}", url, text);
    let result = serde_json::from_str(&text)?;
    Ok(result)
}

async fn get_best_current_price_for(
    server_data: ServerData,
    name: String,
) -> Result<Option<ItadGameInfo>> {
    let name: String = name
        .chars()
        .map(|x| if x.is_alphanumeric() { x } else { ' ' })
        .map(|x| {
            if x == ' ' {
                "%20".to_string()
            } else {
                x.to_string()
            }
        })
        .map(|x| x.chars().collect::<Vec<char>>())
        .flatten()
        .collect();

    let url = format!(
        "{}?key={}&q={}&country=RO",
        ITAD_URL, server_data.config.itad_key, name
    );
    let result: ItadSearchResponse =
        do_get_request(server_data.reqwest_client.clone(), &url).await?;

    Ok(result.data.list.into_iter().next())
}

#[derive(Clone, Debug, Deserialize)]
struct SteamGame {
    appid: u64,
    name: String,
    playtime_forever: u64,
    img_icon_url: String,
    img_logo_url: String,
}

#[derive(Debug, Deserialize)]
struct SteamGamesOwnedActualResponse {
    games: Vec<SteamGame>,
}

#[derive(Debug, Deserialize)]
struct SteamGamesOwnedResponse {
    response: SteamGamesOwnedActualResponse,
}

async fn get_owned_games(server_data: ServerData, id: u64) -> Result<Vec<SteamGame>> {
    {
        let cache = &*server_data.cache.lock().await;
        if let Some(x) = cache.steam_user_games.get(&id) {
            return Ok(x.clone())
        }
    }

    let url = STEAM_GET_OWNED_GAMES_URL
        .replace("{steam_key}", &server_data.config.steam_key)
        .replace("{steam_id}", &id.to_string());

    let response: SteamGamesOwnedResponse =
        do_get_request(server_data.reqwest_client.clone(), &url).await?;

    let cache = &mut *server_data.cache.lock().await;
    cache.steam_user_games.insert(id, response.response.games.clone());

    Ok(response.response.games)
}

#[derive(Clone, Debug, Deserialize)]
struct SteamPlayerInfo {
    personaname: String,
    avatar: String,
}

#[derive(Debug, Deserialize)]
struct SteamPlayersInfo {
    players: Vec<SteamPlayerInfo>,
}

#[derive(Debug, Deserialize)]
struct SteamUserInfoResponse {
    response: SteamPlayersInfo,
}

async fn get_steam_user_info(
    server_data: ServerData,
    steam_id: u64,
) -> Result<Option<SteamPlayerInfo>> {
    {
        let cache = &*server_data.cache.lock().await;
        if let Some(x) = cache.steam_user_info.get(&steam_id) {
            return Ok(x.clone());
        }
    }

    let url = STEAM_GET_USER_INFO_URL
        .replace("{steam_key}", &server_data.config.steam_key)
        .replace("{steam_id}", &steam_id.to_string());

    let info: SteamUserInfoResponse =
        do_get_request(server_data.reqwest_client.clone(), &url).await?;
    let result = info.response.players.into_iter().next();

    let cache = &mut *server_data.cache.lock().await;
    cache.steam_user_info.insert(steam_id, result.clone());

    Ok(result)
}

#[derive(Debug, Serialize)]
struct SendGridFrom {
    email: String,
}

#[derive(Debug, Serialize)]
struct SendGridPersonalizations {
    subject: String,
    to: Vec<SendGridFrom>,
}

#[derive(Debug, Serialize)]
struct SendGridContent {
    #[serde(rename = "type")]
    content_type: String,
    value: String,
}

#[derive(Debug, Serialize)]
struct SendGridSend {
    from: SendGridFrom,
    personalizations: Vec<SendGridPersonalizations>,
    content: Vec<SendGridContent>,
}

async fn send_email(server_data: ServerData, content: String) -> Result<()> {
    let bearer = format!("Bearer {}", server_data.config.sendgrid_key);
    let body = SendGridSend {
        from: SendGridFrom {
            email: "spam@tachyon.tk".to_string(),
        },
        personalizations: vec![SendGridPersonalizations {
            subject: "Hello".to_string(),
            to: vec![SendGridFrom {
                email: server_data.config.email_address.clone(),
            }],
        }],
        content: vec![SendGridContent {
            content_type: "text/plain".to_string(),
            value: content.to_string(),
        }],
    };
    let body = serde_json::to_string(&body)?;

    let request = server_data
        .reqwest_client
        .post(SENDGRID_SEND_URL)
        .header("Authorization", bearer)
        .header("Content-Type", "application/json")
        .body(body)
        .send()
        .await?
        .text()
        .await?;
    println!("{}", request);

    Ok(())
}

#[derive(Debug, Default, Serialize)]
struct SingleRequestResponse {
    user_name: Option<String>,
    user_avatar: Option<String>,
    game_name: Option<String>,
    play_time: Option<u64>,
    icon: Option<String>,
    price: Option<f64>,
    itad_link: Option<String>,

    latency_steam_user_info: u128,
    latency_steam_games: u128,
    latency_itad: u128,
    latency_sendgrid: u128,
    latency_total: u128,
}

async fn do_single_request(
    server_data: ServerData,
    steam_id: u64,
) -> Result<SingleRequestResponse> {
    let mut response = SingleRequestResponse::default();
    let start_total = Instant::now();

    let start = Instant::now();
    let user_info = get_steam_user_info(server_data.clone(), steam_id).await?;
    response.latency_steam_user_info = start.elapsed().as_millis();

    let start = Instant::now();
    let mut steam_game = get_owned_games(server_data.clone(), steam_id).await?;
    response.latency_steam_games = start.elapsed().as_millis();

    steam_game.sort_by(|x, y| y.playtime_forever.cmp(&x.playtime_forever));
    let steam_game = steam_game.into_iter().next();

    let itad_price = match &steam_game {
        Some(x) => {
            let start = Instant::now();
            let result = get_best_current_price_for(server_data.clone(), x.name.clone()).await?;
            response.latency_itad = start.elapsed().as_millis();
            result
        }
        None => None,
    };

    if let Some(user_info) = user_info {
        response.user_name = Some(user_info.personaname);
        response.user_avatar = Some(user_info.avatar);
    }
    if let Some(steam_game) = steam_game {
        let icon_url = format!(
            "http://media.steampowered.com/steamcommunity/public/images/apps/{}/{}.jpg",
            steam_game.appid, steam_game.img_icon_url
        );

        response.game_name = Some(steam_game.name);
        response.play_time = Some(steam_game.playtime_forever);
        response.icon = Some(icon_url);
    }
    if let Some(itad_price) = itad_price {
        response.price = Some(itad_price.price_new);
        response.itad_link = Some(itad_price.urls.game);
    }

    let result = serde_json::to_string_pretty(&response)?;

    let start = Instant::now();
    send_email(server_data, result).await?;
    response.latency_sendgrid = start.elapsed().as_millis();
    response.latency_total = start_total.elapsed().as_millis();

    Ok(response)
}

#[derive(Debug, Serialize)]
struct LogsLatencyInfo {
    minimum: u128,
    average: u128,
    maximum: u128,
}

#[derive(Debug, Serialize)]
struct LogsResponse {
    results: Vec<SingleRequestResponse>,
    steam: LogsLatencyInfo,
    itad: LogsLatencyInfo,
    sendgrid: LogsLatencyInfo,
}

fn calculate_latencies<T, F:Fn(&T) -> u128>(elements: &[T], function: F) -> LogsLatencyInfo {
    let mut minimum = std::u128::MAX;
    let mut sum = 0;
    let mut maximum = 0;

    for i in elements {
        let value = function(i);
        minimum = minimum.min(value);
        maximum = maximum.max(value);
        sum += value;
    }

    let average = sum / elements.len() as u128;
    LogsLatencyInfo {
        minimum,
        average,
        maximum
    }
}

async fn do_logs(server_data: ServerData, sender: &mut Sender) -> Result<String> {
    let mut all_results = Vec::new();
    let mut counter = 0u32;

    for _ in 0..server_data.config.batch_number {
        let result = do_single_request(
            server_data.clone(),
            server_data.config.test_steam_id,
        ).await?;
        all_results.push(result);
    }

    // for _ in 0..server_data.config.number_of_batches {
    //     let mut futures: FuturesUnordered<_> = (0..server_data.config.batch_number)
    //         .map(|_| {
    //             do_single_request(
    //                 server_data.clone(),
    //                 server_data.config.test_steam_id,
    //             )
    //         })
    //         .collect();
    //
    //     while let Ok(Some(completed)) = futures.try_next().await {
    //         all_results.push(completed);
    //
    //         let string = format!("done #{}\n", counter);
    //         println!("{}", string);
    //         let vector = string.into_bytes();
    //         let bytes = Bytes::from(vector);
    //         sender.send_data(bytes).await?;
    //
    //         counter += 1;
    //     }
    // }

    let steam = calculate_latencies(&all_results, |x| x.latency_steam_user_info);
    let itad = calculate_latencies(&all_results, |x| x.latency_itad);
    let sendgrid = calculate_latencies(&all_results, |x| x.latency_sendgrid);

    let logs = LogsResponse {
        results: all_results,
        steam,
        itad,
        sendgrid
    };
    let logs = serde_json::to_string_pretty(&logs)?;
    fs::write("logs.json", &logs).await?;

    Ok(logs)
}

#[derive(Debug, Serialize)]
struct ItadLogsResponse {
    results: Vec<ItadGameInfo>,
    itad: LogsLatencyInfo,
}

async fn test_itad(server_data: ServerData) -> Result<String> {
    let content = fs::read_to_string("games.json").await?;
    let games_names = content.split("\n");

    let start = Instant::now();
    let mut futures: FuturesUnordered<_> = games_names.map(|x| (get_best_current_price_for(server_data.clone(), x.to_string()))).collect();
    let mut results = Vec::new();
    let mut latencies = Vec::new();

    while let Some(Some(x)) = futures.try_next().await? {
        results.push(x);
        latencies.push(start.elapsed().as_millis());
    }

    let latencies = calculate_latencies(&latencies, |x| *x);
    let response = ItadLogsResponse {
        results,
        itad: latencies
    };
    let result = serde_json::to_string_pretty(&response)?;

    Ok(result)
}

async fn test_sendgrid(server_data: ServerData) -> Result<String> {
    let start = Instant::now();
    let mut futures: FuturesUnordered<_> = (1..=100).map(|x| (send_email(server_data.clone(), x.to_string()))).collect();
    let mut latencies = Vec::new();

    while let Some(_) = futures.try_next().await? {
        latencies.push(start.elapsed().as_millis());
    }

    let latencies = calculate_latencies(&latencies, |x| *x);
    let response = ItadLogsResponse {
        results: vec![],
        itad: latencies
    };
    let result = serde_json::to_string_pretty(&response)?;

    Ok(result)
}

async fn do_requests(
    server_data: ServerData,
    request: Request<Body>,
    sender: &mut Sender,
) -> Result<Vec<u8>> {
    let path = match request.uri().path() {
        "/" => "index.html",
        path => &path[1..],
    };
    let parameters: HashMap<String, String> =
        form_urlencoded::parse(request.uri().query().unwrap_or_default().as_bytes())
            .into_owned()
            .collect();

    let result = if path.starts_with("one_request") {
        let steam_id: u64 = parameters
            .get("id_field")
            .ok_or(anyhow!("can't unwrap id_field"))?
            .parse()?;
        let response = do_single_request(server_data, steam_id).await?;
        serde_json::to_string_pretty(&response)?.into()
    } else if path == "logs" {
        do_logs(server_data, sender).await?.into()
    } else if path == "itad_test" {
        test_itad(server_data).await?.into()
    } else if path == "test_sendgrid" {
        test_sendgrid(server_data).await?.into()
    } else {
        fs::read(path).await?
    };

    Ok(result)
}

async fn do_responses(request: Request<Body>, server_data: ServerData) -> Result<Response<Body>> {
    let (mut sender, body) = Body::channel();
    let bytes = match do_requests(server_data, request, &mut sender).await {
        Ok(x) => x,
        Err(x) => format!("{:?}", x).into(),
    };
    sender.send_data(Bytes::from(bytes)).await?;

    Ok(Response::new(body))
}

async fn read_config() -> Result<Configuration> {
    let string = fs::read_to_string("config.toml").await?;
    Ok(toml::from_str(&string)?)
}

#[derive(Default)]
struct ServerCache {
    steam_user_info: HashMap<u64, Option<SteamPlayerInfo>>,
    steam_user_games: HashMap<u64, Vec<SteamGame>>,
}

struct ServerDataStruct {
    config: Configuration,
    reqwest_client: ReqwestClient,
    cache: Mutex<ServerCache>,
}

type ServerData = Arc<ServerDataStruct>;

#[tokio::main]
async fn main() -> Result<()> {
    let mut headers = HeaderMap::new();
    headers.insert(
        header::ACCEPT_LANGUAGE,
        HeaderValue::from_static("en-US,en;q=0.5"),
    );
    headers.insert(header::ACCEPT_ENCODING, HeaderValue::from_static("br"));

    let reqwest_client = ReqwestClient::builder().default_headers(headers).build()?;
    let config = read_config().await?;

    let address = config.listen_on.parse()?;

    let server_data = ServerData::new(ServerDataStruct {
        config,
        reqwest_client,
        cache: Mutex::new(ServerCache::default()),
    });

    let new_service = make_service_fn(move |_| {
        let server_data = server_data.clone();
        async {
            Ok::<_, anyhow::Error>(service_fn(move |req| {
                do_responses(req, server_data.clone())
            }))
        }
    });

    let server = Server::bind(&address).serve(new_service);

    println!("Listening on http://{}", address);

    server.await?;

    Ok(())
}
