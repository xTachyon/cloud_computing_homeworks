create unlogged table if not exists public.domains (
    domain_id bigserial primary key,
    dns text not null
);

create unlogged table if not exists public.categories (
    category_id bigserial primary key,
    category_name text
);

create unlogged table if not exists public.category_mapping (
    domain_id bigint references domains(domain_id),
    category_id bigint unique references categories(category_id)
);